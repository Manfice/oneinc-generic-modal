import { Injectable } from '@angular/core';

import { StorageService } from '@lib-common/services/storage.service';

import {
  IScreenflow,
  IPayScreenflowMetadata,
  IScreenflowStep,
  IPayFromCard,
  IPayFromBankAccount,
  IPaymentMethodData,
  IWidget,
  IAddPaymentMethodScreenflowMetadata,
} from '@lib-common/interfaces';

import {
  HeaderWidgetModel,
  PciDisclaimerWidgetModel,
  PaymentAmountWidgetModel,
  ControlWidgetModel,
  PaymentMethodWidgetModel,
  PaymentTypeWidgetModel,
} from '@lib-common/models';

import {
  ScreenflowTypeEnum,
  PaymentTypeEnum,
  PaymentTypeIconsEnum,
} from '@lib-common/enums';

@Injectable()
export class FakeScreenflowService {

  constructor(
    private readonly store: StorageService,
  ) {
  }

  private createPayScreenflow(): IScreenflow {
    const screenflow: IScreenflow<IPayScreenflowMetadata> = {
      screenflowType: ScreenflowTypeEnum.Pay,
      screenflowMetadata: {
        title: 'Payment',
      },
      stepList: [],
    };

    this.fillPayScreenflow(screenflow);

    return screenflow;
  }

  private fillPayScreenflow(screenflow: IScreenflow<IPayScreenflowMetadata>): void {
    const screenflowStep: IScreenflowStep = {
        widgetsList: [],
        onCancel: null,
        onNext: null,
    };

    screenflowStep.widgetsList.push(new HeaderWidgetModel().register());
    screenflowStep.widgetsList.push(new PciDisclaimerWidgetModel().register());
    screenflowStep.widgetsList.push(new PaymentAmountWidgetModel().register());
    screenflowStep.widgetsList.push(this.fillPaymentMethods());
    screenflowStep.widgetsList.push(new ControlWidgetModel().register());

    screenflow.stepList.push(screenflowStep);
  }

  private fillPaymentMethods(): PaymentMethodWidgetModel {
    const widget = new PaymentMethodWidgetModel();

    const creditCardMethod: IPayFromCard = {
      id: '1',
      emitter: 'MasterCard',
      expirationMonth: 8,
      expirationYear: 2027,
      isDefault: true,
      lastForDigits: '3443',
      logo: '',
      paymentType: PaymentTypeEnum.CreditCard,
    };

    const bancAccount: IPayFromBankAccount = {
      id: '2',
      bankName: 'Western Union',
      isDefault: false,
      lastFourAccountDigits: '6675',
      logo: '',
      paymentType: PaymentTypeEnum.BankAccount,
    };

    const widgetData: IPaymentMethodData = {
      title: 'Select payment method',
      widgetTitle: 'Payment methods widget',
      methods: [creditCardMethod, bancAccount],
    };

    widget.widgetData = widgetData;

    return widget;
  }

  private createAddPaymentMethodScreenflow(): IScreenflow {
    const screenflow: IScreenflow<IAddPaymentMethodScreenflowMetadata> = {
      screenflowMetadata: {
        title: 'Add payment method',
      },
      screenflowType: ScreenflowTypeEnum.AddPaymentMethod,
      stepList: [
        {
          widgetsList: this.fillAddPaymentWidgets(),
          onCancel: null,
          onNext: null,
        },
      ],
    };

    return screenflow;
  }

  private fillAddPaymentWidgets(): Array<IWidget> {
    const widgetList: Array<IWidget> = [];
    const header = new HeaderWidgetModel();
    header.setWidgetData({
      title: 'Add payment method',
      widgetTitle: 'Header widget',
    });
    widgetList.push(header);

    const paymentType = new PaymentTypeWidgetModel();
    paymentType.setWidgetData({
      paymentTypes: [
        {
          icon: PaymentTypeIconsEnum.CreditCard,
          paymentType: PaymentTypeEnum.CreditCard,
          selected: true,
          title: 'Credit card',
        },
        {
          icon: PaymentTypeIconsEnum.BancAccount,
          paymentType: PaymentTypeEnum.BankAccount,
          selected: false,
          title: 'Bank account',
        },
      ],
      widgetTitle: 'Payment type widget',
    });
    widgetList.push(paymentType);

    const actions = new ControlWidgetModel();
    widgetList.push(actions);

    return widgetList;
  }

  /** Create and save payment screenflow */
  private initPayScreenflow(): void {
    const payScreenflow = this.createPayScreenflow();
    this.store.set<IScreenflow<IPayScreenflowMetadata>>(payScreenflow.screenflowType, payScreenflow);
  }

  /** Create and save add payment method screenflow */
  private initAddPaymentMethodScreenflow(): void {
    const addPaymentMethod = this.createAddPaymentMethodScreenflow();
    this.store.set(addPaymentMethod.screenflowType, addPaymentMethod);
  }

  public initializeScreenflows(): Promise<any> {

    return new Promise((resolveInit: any, _: any): any => {
      this.store.initLocalStorage();
      this.initPayScreenflow();
      this.initAddPaymentMethodScreenflow();

      resolveInit();
    });
  }
}
