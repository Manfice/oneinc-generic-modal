import { Component, OnInit, Type } from '@angular/core';

// Interfaces
import { IWidget } from '@lib-common/interfaces';

// Services
import { WidgetControlService } from '@lib-common/services';

@Component({
  selector: 'oinc-widget-list',
  templateUrl: './widget-list.component.html',
  styleUrls: ['./widget-list.component.scss'],
})
export class WidgetListComponent implements OnInit {
  public widgetList: Array<IWidget> = [];
  public activeWidget: string;

  constructor(
    private readonly ws: WidgetControlService,
  ) {
  }

  public setActive(widget: IWidget): void {
    this.activeWidget = widget.widgetComponentName;
  }

  private initWidgetList(): void {
    this.ws.getWidgetList$()
      .subscribe((widgetList: Array<IWidget>) => this.widgetList = widgetList);
  }

  public ngOnInit(): void {
    this.initWidgetList();
  }
}
