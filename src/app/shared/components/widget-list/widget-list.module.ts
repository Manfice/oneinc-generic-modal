import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';

// Components
import { WidgetListComponent } from './widget-list/widget-list.component';

// Modules
import { DynamicRendererModule } from '@lib-common';

@NgModule({
  declarations: [
    WidgetListComponent,
  ],
  imports: [
    CommonModule,
    DynamicRendererModule,
    DragDropModule,
  ],
  exports: [
    WidgetListComponent,
  ],
})
export class WidgetListModule { }
