import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenflowConfigureComponent } from './screenflow-configure.component';

describe('ScreenflowConfigureComponent', () => {
  let component: ScreenflowConfigureComponent;
  let fixture: ComponentFixture<ScreenflowConfigureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreenflowConfigureComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenflowConfigureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
