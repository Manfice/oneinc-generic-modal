import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'oinc-screenflow-configure',
  templateUrl: './screenflow-configure.component.html',
  styleUrls: ['./screenflow-configure.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScreenflowConfigureComponent implements OnInit {
  public ngOnInit(): void {
  }
}
