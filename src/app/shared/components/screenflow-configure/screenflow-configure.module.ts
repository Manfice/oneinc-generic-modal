import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { ScreenflowConfigureComponent } from './screenflow-configure.component';

// Modules
import {
  HeaderWidgetModule,
  PciDisclaimerWidgetModule,
  PaymentTypeWidgetModule,
  ControlsWidgetModule,
  PaymentAmountWidgetModule,
  PaymentMethodWidgetModule,
} from '@lib-common';

// Const
import { widgets } from '@lib-common/constants';
import { ScreenflowConfigureRoutingModule } from './screenflow-configure-routing.module';
import { WidgetListModule } from '../widget-list/widget-list.module';

@NgModule({
  declarations: [
    ScreenflowConfigureComponent,
  ],
  imports: [
    CommonModule,
    ScreenflowConfigureRoutingModule,
    WidgetListModule,

    // widgets
    HeaderWidgetModule,
    PciDisclaimerWidgetModule,
    PaymentTypeWidgetModule,
    ControlsWidgetModule,
    PaymentAmountWidgetModule,
    PaymentMethodWidgetModule,
  ],
  exports: [
    ScreenflowConfigureComponent,
  ],
  entryComponents: [
    ...widgets,
  ],
})
export class ScreenflowConfigureModule { }
