import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScreenflowConfigureComponent } from './screenflow-configure.component';

const routes: Routes = [
  {
    path: '',
    component: ScreenflowConfigureComponent,
    children: [
      {
        path: 'gm/:type',
        loadChildren: () => import('@lib-common/modules/generic-modal-app/generic-modal-app.module')
          .then(m => m.GenericModalAppModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScreenflowConfigureRoutingModule {}
