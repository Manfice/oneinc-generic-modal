import { APP_INITIALIZER } from '@angular/core';
import { FakeScreenflowService } from '../services';

export function screenflowFactory(fss: FakeScreenflowService): any {
  return (): any => fss.initializeScreenflows();
}

export const fakeScreenflowInitializer = {
  provide: APP_INITIALIZER,
  useFactory: screenflowFactory,
  deps: [FakeScreenflowService],
  multi: true,
};
