import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgZorroAntdModule, NZ_I18N, en_US, NzLayoutModule } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';

import en from '@angular/common/locales/en';
import { fakeScreenflowInitializer } from './shared/initializers';
import { FakeScreenflowService } from './shared/services';
import { StorageService } from '@lib-common/services/storage.service';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzLayoutModule,
  ],
  providers: [
    {
      provide: NZ_I18N,
      useValue: en_US,
    },
    FakeScreenflowService,
    StorageService,

    // Initializers
    fakeScreenflowInitializer,
  ],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule {
}
