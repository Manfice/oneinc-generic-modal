import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScreenflowRoutingModule } from './screenflow-routing.module';

// Services
import { WidgetControlService } from '@lib-common/services';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    ScreenflowRoutingModule,
  ],
  providers: [
    WidgetControlService,
  ],
})
export class ScreenflowModule { }
