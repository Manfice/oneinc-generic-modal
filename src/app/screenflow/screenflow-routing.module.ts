import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'configure',
    loadChildren: () => import('../shared/components/screenflow-configure/screenflow-configure.module')
      .then(m => m.ScreenflowConfigureModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScreenflowRoutingModule { }
