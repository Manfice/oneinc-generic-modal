import { Component } from '@angular/core';

@Component({
  selector: 'oinc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {

  public sideMenuCollapsed: boolean = false;
}
