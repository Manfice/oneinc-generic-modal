import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'ol-generic-modal-container',
  templateUrl: './generic-modal-container.component.html',
  styleUrls: ['./generic-modal-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenericModalContainerComponent implements OnInit {

  public ngOnInit(): void {
  }
}
