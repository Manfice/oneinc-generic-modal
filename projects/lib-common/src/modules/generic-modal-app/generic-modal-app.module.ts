import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { GenericModalAppRoutingModule } from './generic-modal-app-routing.module';
import {
  HeaderWidgetModule,
  PciDisclaimerWidgetModule,
  PaymentTypeWidgetModule,
  ControlsWidgetModule,
  PaymentAmountWidgetModule,
  PaymentMethodWidgetModule,
} from '@lib-common/components';

// Components
import { GenericModalContainerComponent } from './generic-modal-container/generic-modal-container.component';

// Constants
import { widgets } from '@lib-common/constants/widgets.list';
import { FakeScreenflowDataService, ScreenflowService } from '@lib-common/services';
import { PaymentMethodService } from '@lib-common/components/widgets/payment-method-widget/services';

// Services

@NgModule({
  declarations: [
    GenericModalContainerComponent,
  ],
  imports: [
    CommonModule,
    GenericModalAppRoutingModule,

    // widgets
    HeaderWidgetModule,
    PciDisclaimerWidgetModule,
    PaymentTypeWidgetModule,
    ControlsWidgetModule,
    PaymentAmountWidgetModule,
    PaymentMethodWidgetModule,
  ],
  providers: [
    FakeScreenflowDataService,
    ScreenflowService,
    PaymentMethodService,
  ],
  entryComponents: [
    ...widgets,
  ],
})
export class GenericModalAppModule { }
