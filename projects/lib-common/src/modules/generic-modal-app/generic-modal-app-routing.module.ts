import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GenericModalContainerComponent } from './generic-modal-container/generic-modal-container.component';

const routes: Routes = [
  {
    path: '',
    component: GenericModalContainerComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('../generic-modal/generic-modal.module')
          .then(m => m.GenericModalModule),
      },
      {
        path: 'add-payment-method',
        loadChildren: () => import('../../components/widgets/payment-method-widget/add-payment-method/add-payment-method.module')
          .then(m => m.AddPaymentMethodModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GenericModalAppRoutingModule { }
