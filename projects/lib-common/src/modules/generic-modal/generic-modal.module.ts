import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GenericModalRoutingModule } from './generic-modal-routing.module';

// Components
import { GenericModalComponent } from './components';

// Modules
import { ScreenflowRendererModule } from '@lib-common/components/screenflow-renderer';

@NgModule({
  declarations: [
    GenericModalComponent,
  ],
  imports: [
    CommonModule,
    GenericModalRoutingModule,
    ScreenflowRendererModule,
  ],
  exports: [
  ],
})
export class GenericModalModule { }
