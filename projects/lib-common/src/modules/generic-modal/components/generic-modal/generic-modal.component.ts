import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
} from '@angular/core';
import { ActivatedRoute, ParamMap, ActivatedRouteSnapshot } from '@angular/router';

// Interfaces
import { IScreenflow, IScreenflowStep } from '@lib-common/interfaces';

// Services
import { ScreenflowService } from '@lib-common/services';

// Rxjs
import { switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'ol-generic-modal',
  templateUrl: './generic-modal.component.html',
  styleUrls: ['./generic-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenericModalComponent implements OnInit {
  public screenflow: IScreenflow;
  public activeStep: IScreenflowStep;

  constructor(
    private readonly ar: ActivatedRoute,
    private readonly sfs: ScreenflowService,
  ) {
  }

  public ngOnInit(): void {
    this.initScreenflow();
  }

  private initScreenflow(): void {
    this.ar.paramMap
      .pipe(
        switchMap((params: ParamMap) => {
          const screenflowName = params.get('type');

          return this.sfs.getScreenflowByName(screenflowName);
        }),
        tap((screenflow: IScreenflow) => {
          this.activeStep = screenflow.stepList
            .find((step: IScreenflowStep) => step !== undefined);
        }),
      )
      .subscribe((screenflow: IScreenflow) => {
        this.screenflow = screenflow;
      });
  }
}
