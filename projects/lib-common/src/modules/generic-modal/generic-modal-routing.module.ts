import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { GenericModalComponent } from './components';

const routes: Routes = [
  {
    path: '',
    component: GenericModalComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GenericModalRoutingModule { }
