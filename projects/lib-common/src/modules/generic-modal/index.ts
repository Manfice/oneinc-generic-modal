export * from './generic-modal.module';
export * from './generic-modal-routing.module';

export * from './components';
