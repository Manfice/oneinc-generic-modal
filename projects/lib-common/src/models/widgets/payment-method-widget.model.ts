// Models
import { WidgetBase } from '../abstract';

// Interfaces
import { IWidget, IPaymentMethodData, IWidgetMetadata } from '@lib-common/interfaces';

export class PaymentMethodWidgetModel extends WidgetBase implements IWidget {
  public readonly widgetComponentName = 'PaymentMethodWidgetComponent';
  public widgetData: IPaymentMethodData;
  public widgetCountable: boolean = true;

  public register(): IWidget {
    const widget = new PaymentMethodWidgetModel();

    widget.widgetData = {
      widgetTitle: 'Payment method widget',
      title: 'Select Payment Method',
      methods: [],
    };

    return widget;
  }

  public setWidgetData(data: IWidgetMetadata): void {
    throw new Error('Method not implemented.');
  }
}
