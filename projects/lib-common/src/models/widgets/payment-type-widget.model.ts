// Models
import { WidgetBase } from '../abstract';

// Interfaces
import {
  IWidget,
  IPaymentTypeData
} from '@lib-common/interfaces';

// Enums
import { PaymentTypeIconsEnum, PaymentTypeEnum } from '@lib-common/enums';

export class PaymentTypeWidgetModel extends WidgetBase implements IWidget<IPaymentTypeData> {
  public readonly widgetComponentName = 'PaymentTypeWidgetComponent';
  public widgetData: IPaymentTypeData;
  public widgetCountable: boolean = false;

  public register(): IWidget {
    const widget = new PaymentTypeWidgetModel();

    widget.widgetData = {
      widgetTitle: 'Payment type widget',
      paymentTypes: [
        { paymentType: PaymentTypeEnum.CreditCard, title: 'Credit card', icon: PaymentTypeIconsEnum.CreditCard, selected: false },
        { paymentType: PaymentTypeEnum.BankAccount, title: 'Bank account', icon: PaymentTypeIconsEnum.BancAccount, selected: true },
      ],
      widgetForm: undefined,
    };

    return widget;
  }

  public setWidgetData(data: IPaymentTypeData): void {
    this.widgetData = data;
  }
}
