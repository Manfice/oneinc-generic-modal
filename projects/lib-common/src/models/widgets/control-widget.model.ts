// Models
import { WidgetBase } from '../abstract';

// Interfaces
import { IWidget, IWidgetMetadata } from '@lib-common/interfaces';

export class ControlWidgetModel extends WidgetBase implements IWidget {
  public readonly widgetComponentName = 'ControlWidgetComponent';
  public widgetData: IWidgetMetadata;
  public widgetCountable: boolean = false;

  public register(): IWidget {
    const widget = new ControlWidgetModel();

    widget.widgetData = {
      widgetTitle: 'Control widget',
    };

    return widget;
  }
  public setWidgetData(data: any): void {
    throw new Error('Method not implemented.');
  }
}
