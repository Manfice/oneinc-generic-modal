// Models
import { WidgetBase } from '../abstract';

// Interfaces
import { IWidget, IWidgetMetadata, IPaymentAmountData } from '@lib-common/interfaces';

/** Payment amount widget model */
export class PaymentAmountWidgetModel extends WidgetBase implements IWidget {
  public readonly widgetComponentName = 'PaymentAmountWidgetComponent';
  public widgetData: IPaymentAmountData;
  public widgetCountable: boolean = true;

  public register(): IWidget {
    const widget = new PaymentAmountWidgetModel();

    widget.widgetData = {
      widgetTitle: 'Payment Amount widget',
      title: 'Select Payment Amount',
      amount: 210.4,
    };

    return widget;
  }

  public setWidgetData(data: IWidgetMetadata): void {
    throw new Error('Method not implemented.');
  }
}
