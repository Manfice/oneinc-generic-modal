// Models
import { WidgetBase } from '../abstract';

// Interfaces
import {
  IHeaderWidgetData,
  IWidget,
} from '@lib-common/interfaces';

export class HeaderWidgetModel extends WidgetBase implements IWidget<IHeaderWidgetData> {
  public readonly widgetComponentName = 'HeaderWidgetComponent';
  public widgetData: IHeaderWidgetData;
  public widgetCountable: boolean = false;

  public register(): IWidget {
    const widget = new HeaderWidgetModel();

    widget.widgetData = {
      title: 'Payment process',
      widgetTitle: 'Header widget',
    };

    return widget;
  }

  public setWidgetData(data: IHeaderWidgetData): void {
    this.widgetData = data;
  }
}
