// Interfaces
import { IWidget, IPciDisclaimerData } from '@lib-common/interfaces';

// Models
import { WidgetBase } from '../abstract';

export class PciDisclaimerWidgetModel extends WidgetBase implements IWidget<IPciDisclaimerData> {
  public readonly widgetComponentName = 'PciDisclaimerWidgetComponent';
  public widgetData: IPciDisclaimerData;
  public widgetCountable: boolean = false;

  public register(): IWidget {
    const widget = new PciDisclaimerWidgetModel();

    widget.widgetData = {
      disclaimerText: 'Your data is secured.',
      widgetTitle: 'Pci disclaimer widget',
    };

    return widget;
  }

  public setWidgetData(data: IPciDisclaimerData): void {
    this.widgetData = data;
  }
}
