import { IWidget, IWidgetMetadata } from '@lib-common/interfaces';

/** Base widget class thats allow us register widgets in system on demand */
export abstract class WidgetBase {
  /** Register widget in system */
  public abstract register(): IWidget;

  /** Set widget metadata */
  public abstract setWidgetData(data: IWidgetMetadata): void;
}
