import { Injectable } from '@angular/core';

// Rxjs
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';

// Interfaces
import { IScreenflow, IPayScreenflowMetadata, IScreenflowStep } from '@lib-common/interfaces';

// Services
import { FakeScreenflowDataService } from './fake-data';

/** Service serve screenflow module */
@Injectable()
export class ScreenflowService {
  public onClose = new Subject();
  public onNext = new Subject();
  public currentScreenflow = new BehaviorSubject<IScreenflow>(null);
  public currentStep = new BehaviorSubject<IScreenflowStep>(null);

  private screenflow: IScreenflow;

  constructor(
    private readonly dataService: FakeScreenflowDataService,
  ) {
    this.initSubscribes();
  }

  public getScreenflowByName(screenflowType: string): Observable<IScreenflow<IPayScreenflowMetadata>> {

    return this.dataService.getPayScreenflow(screenflowType)
      .pipe(
        tap((screenflow: IScreenflow) => this.screenflow = screenflow),
      );
  }

  public closeModal(): void {
    this.onClose.next();
  }

  public setCurrentScreenflow(screenflow: IScreenflow): void {
    this.currentScreenflow.next(screenflow);
  }

  private initSubscribes(): void {
    this.currentScreenflow
      .subscribe((screenflow: IScreenflow) => {
        if (!screenflow) {
          return;
        }

        const firstStep = screenflow.stepList
          .find((step: IScreenflowStep) => step !== undefined);
        this.currentStep.next(firstStep);
      });
  }
}
