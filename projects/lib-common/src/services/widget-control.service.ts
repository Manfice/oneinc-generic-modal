import { Injectable } from '@angular/core';

// Interfaces
import { IWidget } from '../interfaces';

// Constants
import { WidgetModelListConstant } from '../constants';

// Models
import { WidgetBase } from '../models/abstract';

// Rxjs
import { Observable, of } from 'rxjs';

@Injectable()
export class WidgetControlService {
  private readonly widgetModelList = WidgetModelListConstant;
  private widgetList: Array<IWidget> = [];

  constructor() {
    this.initWidgetList();
  }

  private initWidgetList(): void {
    this.widgetList = [];
    this.widgetModelList.forEach((widget: WidgetBase) => this.widgetList.push(widget.register()));
  }

  /** Returns widgets list as observable */
  public getWidgetList$(): Observable<Array<IWidget>> {
    return of(this.widgetList);
  }

  /** Returns widgets list */
  public getWidgetList(): Array<IWidget> {
    return this.widgetList;
  }
}
