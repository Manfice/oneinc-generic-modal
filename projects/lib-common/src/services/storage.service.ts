import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root'})
export class StorageService {

  constructor() {
    if (!localStorage) {
      throw new Error('Local storage is not available');
    }
  }

  /**
   * Save data in localStorage
   * @param token Object key
   * @param data Object to save
   */
  public set<T>(token: string, data: T): void {
    const dataString = JSON.stringify(data);
    localStorage.setItem(token, dataString);
  }

  /**
   * Get object from storage
   * @param token Object key
   */
  public get<T>(token: string): T {
    const dataString = localStorage.getItem(token);

    try {
      return JSON.parse(dataString);
    } catch (error) {
      return null;
    }
  }

  public initLocalStorage(): void {
    localStorage.clear();
  }
}
