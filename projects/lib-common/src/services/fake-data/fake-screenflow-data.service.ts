import { Injectable } from '@angular/core';

// Rxjs
import { Observable, of } from 'rxjs';

// Interfaces
import {
  IScreenflow,
} from '@lib-common/interfaces';

// Enums
import { StorageService } from '../storage.service';

/** Service configures fake data for screenflow */
@Injectable()
export class FakeScreenflowDataService {

  constructor(
    private readonly store: StorageService,
  ) {
  }

  /** Returns array of payment screenflow */
  public getPayScreenflow(screenflowType: string): Observable<IScreenflow> {
    return of(this.store.get(screenflowType));
  }
}
