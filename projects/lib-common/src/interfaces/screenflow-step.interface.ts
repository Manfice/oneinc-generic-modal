import { IWidget } from './widget.interface';

export interface IScreenflowStep {
  /** Widget collection */
  widgetsList: Array<IWidget>;
  /** Method, witch will executed on next action */
  onNext(): void;
  /** Method, witch will executed on cancel screenflow action */
  onCancel(): void;
}
