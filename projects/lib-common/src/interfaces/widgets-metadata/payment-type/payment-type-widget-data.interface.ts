// Interfaces
import { IWidgetMetadata } from '../widget-metadata.interface';
import { IPaymentTypeDefinition } from './payment-type-definition.interface';
import { FormGroup } from '@angular/forms';

/** Payment type widget metadata */
export interface IPaymentTypeData extends IWidgetMetadata {
  paymentTypes: Array<IPaymentTypeDefinition>;
}
