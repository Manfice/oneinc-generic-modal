import { PaymentTypeIconsEnum, PaymentTypeEnum } from '@lib-common/enums';

/** Payment type options */
export interface IPaymentTypeDefinition {
  paymentType: PaymentTypeEnum;
  title: string;
  icon: PaymentTypeIconsEnum;
  selected: boolean;
}
