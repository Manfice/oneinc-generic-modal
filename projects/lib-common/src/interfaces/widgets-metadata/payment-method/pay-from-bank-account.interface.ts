import { IPaymentMethod } from './payment-method.interface';

// Bank account payment method
export interface IPayFromBankAccount extends IPaymentMethod {
  bankName: string;
  lastFourAccountDigits: string;
}
