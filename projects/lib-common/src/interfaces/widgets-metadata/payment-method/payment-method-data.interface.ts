import { IPaymentMethod } from './payment-method.interface';
import { IWidgetMetadata } from '../widget-metadata.interface';
import { IScreenflow } from '@lib-common/interfaces';

export interface IPaymentMethodData extends IWidgetMetadata {
  title: string;
  methods: Array<IPaymentMethod>;
}
