import { PaymentTypeEnum } from '@lib-common/enums';

export interface IPaymentMethod {
  /** Payment method identifer in GUID format */
  id: string;
  logo: string;
  paymentType: PaymentTypeEnum;
  isDefault: boolean;
}
