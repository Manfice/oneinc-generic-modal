import { IPaymentMethod } from './payment-method.interface';

// Credit card payment method
export interface IPayFromCard extends IPaymentMethod {
  emitter: string;
  lastForDigits: string;
  expirationMonth: number;
  expirationYear: number;
}
