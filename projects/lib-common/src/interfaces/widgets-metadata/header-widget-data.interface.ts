import { IWidgetMetadata } from './widget-metadata.interface';

/** Header widget metadata definition */
export interface IHeaderWidgetData extends IWidgetMetadata {
  title: string;
}
