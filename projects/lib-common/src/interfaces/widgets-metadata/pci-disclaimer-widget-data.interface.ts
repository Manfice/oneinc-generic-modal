import { IWidgetMetadata } from './widget-metadata.interface';

/** PCI disclaimer metadata */
export interface IPciDisclaimerData extends IWidgetMetadata {
  disclaimerText: string;
}
