import { IWidgetMetadata } from './widget-metadata.interface';

export interface IPaymentAmountData extends IWidgetMetadata {
  title: string;
  amount: number;
}
