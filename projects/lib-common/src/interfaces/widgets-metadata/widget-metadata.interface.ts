import { FormGroup } from '@angular/forms';

/** Common widget metadata interface */
export interface IWidgetMetadata {
  /** widget title in editor or in widget list */
  widgetTitle: string;

  /** widget form group */
  widgetForm?: FormGroup;

  /** If widget is countable this will show step number */
  numberInOrder?: number;
}
