/** Dictionary analog */
export interface IHashMap<T> {
  [key: string]: T;
}
