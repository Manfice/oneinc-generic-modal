// Interfaces
import { IWidgetMetadata } from './widgets-metadata';

/** Widget data describe widget state on screenflow */
export interface IWidget<T = IWidgetMetadata> {
  /** Id of widget */
  id?: string;

  /** Widget component used in dynamic renderer */
  // widgetComponent: Type<any>;

  /** Component name */
  widgetComponentName: string;

  /** Data that used in widget */
  widgetData: T;

  /** If true widgets on screenflow will have count in order */
  widgetCountable: boolean;
}
