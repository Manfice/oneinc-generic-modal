// Enums
import { ScreenflowTypeEnum } from '@lib-common/enums';

// Interfaces
import { IScreenflowStep } from './screenflow-step.interface';

/** Interface describe screenflow state and behavior */
export interface IScreenflow<T = any> {
  /** Count of steps in screenflow */
  stepList: Array<IScreenflowStep>;
  /** Type of screenflow */
  screenflowType: ScreenflowTypeEnum;
  /** Incoming info about current screenflow */
  screenflowMetadata: T;
}
