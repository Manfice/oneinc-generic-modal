/** Screenflow pay properties */
export interface IPayScreenflowMetadata {
  /** screenflow title */
  title: string;
}
