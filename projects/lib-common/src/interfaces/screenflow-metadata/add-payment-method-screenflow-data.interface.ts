/** Add payment method screenflow metadata interface */
export interface IAddPaymentMethodScreenflowMetadata {
  title: string;
}
