import { IWidgetMetadata } from './widgets-metadata';

export interface IWidgetComponent {
  data: IWidgetMetadata;
}
