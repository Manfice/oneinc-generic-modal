import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenflowRendererComponent } from './screenflow-renderer.component';

describe('ScreenflowRendererComponent', () => {
  let component: ScreenflowRendererComponent;
  let fixture: ComponentFixture<ScreenflowRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreenflowRendererComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenflowRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
