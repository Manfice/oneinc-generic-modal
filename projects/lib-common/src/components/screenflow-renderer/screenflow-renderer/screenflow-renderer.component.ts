import {
  Component,
  OnInit,
  Input,
} from '@angular/core';
import { CdkDragDrop, moveItemInArray, copyArrayItem } from '@angular/cdk/drag-drop';

// Interfaces
import { IWidget, IScreenflowStep } from '@lib-common/interfaces';

@Component({
  selector: 'ol-screenflow-renderer',
  templateUrl: './screenflow-renderer.component.html',
  styleUrls: ['./screenflow-renderer.component.scss'],
})
export class ScreenflowRendererComponent implements OnInit {
  @Input() public screenflowStep: IScreenflowStep;

  public dropWidget(event: CdkDragDrop<Array<IWidget>>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        this.screenflowStep.widgetsList,
        event.previousIndex,
        event.currentIndex,
      );
    } else {
      copyArrayItem(
        event.previousContainer.data,
        this.screenflowStep.widgetsList,
        event.previousIndex,
        event.currentIndex,
      );
    }
    this.setWidgetsNumberInOrder();
  }

  public removeWidgetFromList(index: number): void {
    this.screenflowStep.widgetsList.splice(index, 1);
  }

  private setWidgetsNumberInOrder(): void {
    if (!this.screenflowStep) {
      return;
    }

    this.screenflowStep.widgetsList
      .filter((widget: IWidget) => widget.widgetCountable)
      .forEach((widget: IWidget, index: number) => {
        widget.widgetData.numberInOrder = index + 1;
      });
  }

  public ngOnInit(): void {
    this.setWidgetsNumberInOrder();
  }
}
