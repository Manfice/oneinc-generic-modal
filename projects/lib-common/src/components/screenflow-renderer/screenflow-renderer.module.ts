import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';

// Components
import { ScreenflowRendererComponent } from './screenflow-renderer/screenflow-renderer.component';

// Directives
import { OincDirectivesModule } from '@lib-common/directives';

// Modules
import { DynamicRendererModule } from '../dynamic-renderer';

@NgModule({
  declarations: [
    ScreenflowRendererComponent,
  ],
  imports: [
    CommonModule,
    DynamicRendererModule,
    OincDirectivesModule,
    DragDropModule,
  ],
  exports: [
    ScreenflowRendererComponent,
  ],
})
export class ScreenflowRendererModule { }
