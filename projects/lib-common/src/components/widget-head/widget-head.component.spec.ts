import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetHeadComponent } from './widget-head.component';

describe('WidgetHeadComponent', () => {
  let component: WidgetHeadComponent;
  let fixture: ComponentFixture<WidgetHeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetHeadComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
