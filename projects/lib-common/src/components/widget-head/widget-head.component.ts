import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'ol-widget-head',
  templateUrl: './widget-head.component.html',
  styleUrls: ['./widget-head.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WidgetHeadComponent {
  @Input() public stepNumber: number;
  @Input() public title: string;
  @Output() public readonly action = new EventEmitter();

  public emitAction(): void {
    this.action.emit();
  }
}
