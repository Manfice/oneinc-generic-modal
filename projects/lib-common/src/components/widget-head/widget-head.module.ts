import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetHeadComponent } from './widget-head.component';
import { NzButtonModule } from 'ng-zorro-antd';

@NgModule({
  declarations: [
    WidgetHeadComponent,
  ],
  imports: [
    CommonModule,
    NzButtonModule,
  ],
  exports: [
    WidgetHeadComponent,
  ],
})
export class WidgetHeadModule { }
