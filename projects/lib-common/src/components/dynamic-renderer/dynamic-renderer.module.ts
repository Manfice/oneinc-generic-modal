import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { DynamicRendererComponent } from './dynamic-renderer.component';

// Directives
import { OincDirectivesModule } from '@lib-common/directives';
import { NzButtonModule } from 'ng-zorro-antd';

@NgModule({
  declarations: [
    DynamicRendererComponent,
  ],
  imports: [
    CommonModule,
    OincDirectivesModule,
    NzButtonModule,
  ],
  exports: [
    DynamicRendererComponent,
  ],
})
export class DynamicRendererModule { }
