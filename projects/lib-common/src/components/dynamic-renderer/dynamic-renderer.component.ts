import {
  Component,
  OnInit,
  Input,
  ComponentFactoryResolver,
  ViewChild,
  Output,
  EventEmitter,
  Type,
} from '@angular/core';

// Interfaces
import { IWidget, IWidgetComponent } from '@lib-common/interfaces';

// Directives
import { WidgetHostDirective } from '@lib-common/directives';
import { widgets } from '@lib-common/constants/widgets.list';

@Component({
  selector: 'ol-dynamic-renderer',
  templateUrl: './dynamic-renderer.component.html',
  styleUrls: ['./dynamic-renderer.component.scss'],
})
export class DynamicRendererComponent implements OnInit {
  @Input() public widget: IWidget;
  @Input() public widgetIndex: number;

  @Output() public readonly widgetRemove = new EventEmitter<number>();

  @ViewChild(WidgetHostDirective, { static: true })
  public componentHost: WidgetHostDirective;
  public status: boolean = false;
  private readonly components: Array<Type<any>> = [
    ...widgets,
  ];

  constructor(
    private readonly cf: ComponentFactoryResolver,
  ) {
  }

  private loadComponent(): void {
    const component = this.components.find(compt => compt.name === this.widget.widgetComponentName);
    const widget = this.cf.resolveComponentFactory(component);
    const host = this.componentHost.viewContainerRef;
    const componentRef = host.createComponent(widget);
    (componentRef.instance as IWidgetComponent).data = this.widget.widgetData;
  }

  public toggleComponentStatus(): void {
    this.status = !this.status;
  }

  public removeWidgetFromList(): void {
    this.widgetRemove.emit(this.widgetIndex);
  }

  public ngOnInit(): void {
    this.loadComponent();
  }
}
