import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentAmountWidgetComponent } from './payment-amount-widget/payment-amount-widget.component';

import { WidgetHeadModule } from '@lib-common/components/widget-head';

@NgModule({
  declarations: [
    PaymentAmountWidgetComponent,
  ],
  imports: [
    CommonModule,
    WidgetHeadModule,
  ],
})
export class PaymentAmountWidgetModule { }
