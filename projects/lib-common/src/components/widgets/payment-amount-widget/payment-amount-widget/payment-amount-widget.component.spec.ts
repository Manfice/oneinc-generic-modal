import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentAmountWidgetComponent } from './payment-amount-widget.component';

describe('PaymentAmountWidgetComponent', () => {
  let component: PaymentAmountWidgetComponent;
  let fixture: ComponentFixture<PaymentAmountWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentAmountWidgetComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentAmountWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
