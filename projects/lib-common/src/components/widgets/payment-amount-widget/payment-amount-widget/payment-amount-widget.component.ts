import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

// Interfaces
import { IWidgetComponent, IPaymentAmountData } from '@lib-common/interfaces';

@Component({
  selector: 'ol-payment-amount-widget',
  templateUrl: './payment-amount-widget.component.html',
  styleUrls: ['./payment-amount-widget.component.scss'],
})
export class PaymentAmountWidgetComponent implements OnInit, IWidgetComponent {
  @Input() public data: IPaymentAmountData;

  public edit(): void {
    console.log('edit');
  }

  public ngOnInit(): void {
  }
}
