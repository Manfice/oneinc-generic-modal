import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

// Interfaces
import { IWidgetComponent, IWidgetMetadata, IScreenflowStep } from '@lib-common/interfaces';

// Services
import { ScreenflowService } from '@lib-common/services/screenflow.service';

@Component({
  selector: 'ol-control-widget',
  templateUrl: './control-widget.component.html',
  styleUrls: ['./control-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ControlWidgetComponent implements OnInit, IWidgetComponent {
  public data: IWidgetMetadata;
  public step: IScreenflowStep;

  constructor(
    private readonly sfs: ScreenflowService,
  ) {
  }

  public ngOnInit(): void {
    this.sfs.currentStep
      .subscribe((step: IScreenflowStep) => {
        this.step = step;
      });
  }
}
