import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlWidgetComponent } from './control-widget.component';

describe('ControlWidgetComponent', () => {
  let component: ControlWidgetComponent;
  let fixture: ComponentFixture<ControlWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlWidgetComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
