import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { ControlWidgetComponent } from './control-widget/control-widget.component';

// Modules
import { NzButtonModule } from 'ng-zorro-antd';

@NgModule({
  declarations: [
    ControlWidgetComponent,
  ],
  imports: [
    CommonModule,
    NzButtonModule,
  ],
})
export class ControlsWidgetModule { }
