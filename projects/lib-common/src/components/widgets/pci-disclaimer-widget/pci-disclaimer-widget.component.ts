import { Component, OnInit } from '@angular/core';

// Interfaces
import {
  IWidgetComponent,
  IPciDisclaimerData
} from '@lib-common/interfaces';

@Component({
  selector: 'ol-pci-disclaimer-widget',
  templateUrl: './pci-disclaimer-widget.component.html',
  styleUrls: ['./pci-disclaimer-widget.component.scss'],
})
export class PciDisclaimerWidgetComponent implements OnInit, IWidgetComponent {
  public data: IPciDisclaimerData;

  public ngOnInit(): void {
  }
}
