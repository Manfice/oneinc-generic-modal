import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PciDisclaimerWidgetComponent } from './pci-disclaimer-widget.component';

describe('PciDisclaimerWidgetComponent', () => {
  let component: PciDisclaimerWidgetComponent;
  let fixture: ComponentFixture<PciDisclaimerWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PciDisclaimerWidgetComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PciDisclaimerWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
