import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { PciDisclaimerWidgetComponent } from './pci-disclaimer-widget.component';

@NgModule({
  declarations: [
    PciDisclaimerWidgetComponent,
  ],
  imports: [
    CommonModule,
  ],
})
export class PciDisclaimerWidgetModule { }
