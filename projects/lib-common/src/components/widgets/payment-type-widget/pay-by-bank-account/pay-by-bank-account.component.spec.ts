import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayByBankAccountComponent } from './pay-by-bank-account.component';

describe('PayByBankAccountComponent', () => {
  let component: PayByBankAccountComponent;
  let fixture: ComponentFixture<PayByBankAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayByBankAccountComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayByBankAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
