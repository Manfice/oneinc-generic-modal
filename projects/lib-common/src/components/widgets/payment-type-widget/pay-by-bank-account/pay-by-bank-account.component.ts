import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { PaymentMethodService } from '../../payment-method-widget/services';

@Component({
  selector: 'ol-pay-by-bank-account',
  templateUrl: './pay-by-bank-account.component.html',
  styleUrls: ['./pay-by-bank-account.component.scss'],
})
export class PayByBankAccountComponent implements OnInit {
  @Input() public baForm: FormGroup;
  public showErrors$ = this.pms.showErrors$;

  constructor(
    private readonly fb: FormBuilder,
    private readonly pms: PaymentMethodService,
    ) {
  }

  private initFrom(): void {
    const form = this.fb.group({
      name: ['Dart Eviler', Validators.required],
      routingNumber: ['8234720', Validators.required],
      account: ['407028104702601', Validators.required],
      confirmAccount: ['407028104702601', Validators.required],
      savings: ['6500', Validators.required],
      isDefault: [false],
    });
    this.baForm.addControl('addPayment', form);
  }

  public ngOnInit(): void {
    this.initFrom();
  }
}
