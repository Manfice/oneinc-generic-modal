import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { PaymentMethodService } from '../../payment-method-widget/services';

@Component({
  selector: 'ol-pay-by-credit-card',
  templateUrl: './pay-by-credit-card.component.html',
  styleUrls: ['./pay-by-credit-card.component.scss'],
})
export class PayByCreditCardComponent implements OnInit {
  @Input() public ccForm: FormGroup;
  public showErrors$ = this.pms.showErrors$;

  constructor(
    private readonly fb: FormBuilder,
    private readonly pms: PaymentMethodService,
  ) {
  }

  private initForm(): void {
    const addPayment = this.fb.group({
      name: ['Zhork Vartanov', Validators.required],
      cardNumber: ['4123 3344 2323 2121', Validators.required],
      expirationDate: ['2025-04-01', Validators.required],
      code: ['321', Validators.required],
      address: ['Krasnodar', Validators.required],
      zip: ['98765', Validators.required],
      isDefault: [false],
    });

    this.ccForm.addControl('addPayment', addPayment);
  }

  public ngOnInit(): void {
    this.initForm();
  }
}
