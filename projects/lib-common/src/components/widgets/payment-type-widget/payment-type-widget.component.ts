import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

// Interfaces
import {
  IWidgetComponent,
  IPaymentTypeData,
  IPaymentTypeDefinition,
} from '@lib-common/interfaces';

// Enums
import { PaymentTypeEnum } from '@lib-common/enums';
import { PaymentMethodService } from '../payment-method-widget/services';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'ol-payment-type-widget',
  templateUrl: './payment-type-widget.component.html',
  styleUrls: ['./payment-type-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentTypeWidgetComponent implements OnInit, IWidgetComponent {
  @Input() public data: IPaymentTypeData;
  public paymentTypes = PaymentTypeEnum;
  public selectedPaymentType: PaymentTypeEnum;

  constructor(
    private readonly fb: FormBuilder,
    private readonly pms: PaymentMethodService,
  ) {
  }

  private initFrom(): void {
    this.data.widgetForm = this.fb.group({
      paymentType: [this.selectedPaymentType, Validators.required],
    });
  }

  private subsOnPaymentTypeControl(): void {
    const ptControl = this.data.widgetForm.get('paymentType');

    ptControl.valueChanges
      .pipe(
        tap(() => this.pms.showErrors$.next(false)),
      )
      .subscribe((paymentType: PaymentTypeEnum) => {
        this.selectedPaymentType = paymentType;
        this.data.widgetForm
          .removeControl('addPayment');
      });
  }

  private initActivePaymentType(): PaymentTypeEnum {
    const selectedPaymentType = this.data.paymentTypes
      .find((paymentType: IPaymentTypeDefinition) => paymentType.selected);

    return selectedPaymentType ? selectedPaymentType.paymentType : undefined;
  }

  public paymentTypeTrackFn(_: number, item: IPaymentTypeDefinition): PaymentTypeEnum {
    return item.paymentType;
  }

  public ngOnInit(): void {
    this.selectedPaymentType = this.initActivePaymentType();
    this.initFrom();
    this.subsOnPaymentTypeControl();
  }
}
