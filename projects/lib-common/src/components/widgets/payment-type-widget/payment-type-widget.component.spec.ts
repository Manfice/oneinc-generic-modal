import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentTypeWidgetComponent } from './payment-type-widget.component';

describe('PaymentTypeWidgetComponent', () => {
  let component: PaymentTypeWidgetComponent;
  let fixture: ComponentFixture<PaymentTypeWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentTypeWidgetComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentTypeWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
