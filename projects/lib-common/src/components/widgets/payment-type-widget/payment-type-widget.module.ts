import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { PaymentTypeWidgetComponent } from './payment-type-widget.component';
import { PayByCreditCardComponent } from './pay-by-credit-card/pay-by-credit-card.component';
import { PayByBankAccountComponent } from './pay-by-bank-account/pay-by-bank-account.component';

// Modules
import { NzIconModule } from 'ng-zorro-antd/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { NzInputModule, NzCheckboxModule } from 'ng-zorro-antd';

@NgModule({
  declarations: [
    PaymentTypeWidgetComponent,
    PayByCreditCardComponent,
    PayByBankAccountComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NzIconModule,
    NzInputModule,
    NzCheckboxModule,
  ],
})
export class PaymentTypeWidgetModule { }
