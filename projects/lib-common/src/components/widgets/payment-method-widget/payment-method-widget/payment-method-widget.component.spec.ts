import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentMethodWidgetComponent } from './payment-method-widget.component';

describe('PaymentMethodWidgetComponent', () => {
  let component: PaymentMethodWidgetComponent;
  let fixture: ComponentFixture<PaymentMethodWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentMethodWidgetComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentMethodWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
