import { Component, OnInit, ChangeDetectorRef, Input, OnDestroy } from '@angular/core';
import { IWidgetComponent, IPaymentMethodData, IPaymentMethod } from '@lib-common/interfaces';
import { PaymentMethodService } from '../services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ol-payment-method-widget',
  templateUrl: './payment-method-widget.component.html',
  styleUrls: ['./payment-method-widget.component.scss'],
})
export class PaymentMethodWidgetComponent implements OnInit, OnDestroy, IWidgetComponent {
  @Input() public data: IPaymentMethodData;

  private readonly subs: Array<Subscription> = [];

  constructor(
    private readonly pms: PaymentMethodService,
    private readonly cd: ChangeDetectorRef,
  ) {
  }

  public edit(): void {
    console.log('payment method');
  }

  private subscribeOnPaymentMethodChange(): void {
    const change = this.pms.paymentMethodChange$
      .subscribe((methodId: string) => {
        this.changePaymentMethod(methodId);
        this.cd.detectChanges();
      });

    this.subs.push(change);
  }

  private subscribeOnAddPaymentMethod(): void {
    const paymentAdd = this.pms.paymentMethodAdded$
      .subscribe((method: IPaymentMethod) => {
        if (!method) {
          return;
        }

        const alreadyExist = this.data.methods
          .find((paymentMethod: IPaymentMethod) => paymentMethod.id === method.id);

        if (alreadyExist) {
          return;
        }

        this.data.methods.push(method);

        if (method.isDefault) {
          this.changePaymentMethod(method.id);
        }
      });

    this.subs.push(paymentAdd);
  }

  private changePaymentMethod(methodId: string): void {
    this.data.methods
      .forEach((method: IPaymentMethod) => {
        method.isDefault = method.id === methodId;
      });
  }

  public ngOnInit(): void {
    this.subscribeOnPaymentMethodChange();
    this.subscribeOnAddPaymentMethod();
  }

  public ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
