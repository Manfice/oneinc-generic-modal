import { Injectable } from '@angular/core';

// Rxjs
import { Subject, Observable, BehaviorSubject, of } from 'rxjs';

// Interfaces
import { IScreenflow, IPaymentMethod } from '@lib-common/interfaces';

// Services
import { FakeScreenflowDataService } from '@lib-common/services/fake-data/fake-screenflow-data.service';
import { ScreenflowTypeEnum } from '@lib-common/enums';

@Injectable()
export class PaymentMethodService {
  public paymentMethodChange$: Subject<string> = new Subject<string>();
  public showErrors$ = new BehaviorSubject<boolean>(false);
  public paymentMethodAdded$ = new BehaviorSubject<IPaymentMethod>(null);

  constructor(
    private readonly fds: FakeScreenflowDataService,
  ) {
  }

  public getAddPaymentData(): Observable<IScreenflow> {
    return this.fds.getPayScreenflow(ScreenflowTypeEnum.AddPaymentMethod);
  }

  public addPaymentMethod(method: IPaymentMethod): void {
    this.paymentMethodAdded$.next(method);
  }
}
