import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup } from '@angular/forms';

// Services
import { PaymentMethodService } from '../../services';
import { ScreenflowService } from '@lib-common/services';

// Interfaces
import { IScreenflow, IScreenflowStep, IWidget, IPaymentMethod, IPayFromCard, IPayFromBankAccount } from '@lib-common/interfaces';
import { ICreditCardForm, IBankAccountFormValue } from '../interfaces';
import { PaymentTypeEnum } from '@lib-common/enums';

@Component({
  selector: 'ol-add-payment-method',
  templateUrl: './add-payment-method.component.html',
  styleUrls: ['./add-payment-method.component.scss'],
})
export class AddPaymentMethodComponent implements OnInit {
  public form: FormGroup;
  public screenflow: IScreenflow;
  public activeStep: IScreenflowStep;

  constructor(
    private readonly pms: PaymentMethodService,
    private readonly sfs: ScreenflowService,
    private readonly location: Location,
  ) { }

  private goBack(): void {
    this.location.back();
  }

  public addMethod(): void {
    const paymentType = this.activeStep.widgetsList
      .find((widget: IWidget) => widget.widgetComponentName === 'PaymentTypeWidgetComponent');

    this.form = paymentType ? paymentType.widgetData.widgetForm : undefined;

    if (this.form && this.form.invalid) {
      this.pms.showErrors$.next(true);

      return;
    }
    const formValue = this.form.value;
    const method = formValue.paymentType === PaymentTypeEnum.CreditCard
      ? this.createCreditCardMethod(formValue.addPayment)
      : this.createBancAccountMethod(formValue.addPayment);

    this.pms.addPaymentMethod(method);

    this.goBack();
  }

  public cancel(): void {
    this.pms.showErrors$.next(false);
    this.goBack();
  }

  private configureScreenflow(): void {
    this.pms.getAddPaymentData()
      .subscribe((screenflow: IScreenflow) => {
        this.screenflow = screenflow;
        this.initFirstStep(screenflow);
        this.sfs.setCurrentScreenflow(screenflow);
      });
  }

  private initFirstStep(screenflow: IScreenflow): void {
    this.activeStep = screenflow.stepList
      .find((step: IScreenflowStep) => step !== undefined);

    this.activeStep.onCancel = this.cancel.bind(this);
    this.activeStep.onNext = this.addMethod.bind(this);
  }

  private createCreditCardMethod(formData: ICreditCardForm): IPaymentMethod {
    const method: IPayFromCard = {
      emitter: 'Visa',
      expirationMonth: Number(formData.expirationDate.split('-')[0]),
      expirationYear: Number(formData.expirationDate.split('-')[1]),
      id: new Date().getMilliseconds().toString(),
      isDefault: formData.isDefault,
      lastForDigits: formData.cardNumber.substring(formData.cardNumber.length - 4),
      logo: undefined,
      paymentType: PaymentTypeEnum.CreditCard,
    };

    return method;
  }

  private createBancAccountMethod(formData: IBankAccountFormValue): IPaymentMethod {
    const method: IPayFromBankAccount = {
      bankName: 'Tax',
      id: new Date().getMilliseconds().toString(),
      isDefault: formData.isDefault,
      lastFourAccountDigits: formData.account.substring(formData.account.length - 4),
      logo: undefined,
      paymentType: PaymentTypeEnum.BankAccount,
    };

    return method;
  }

  public ngOnInit(): void {
    this.configureScreenflow();
  }
}
