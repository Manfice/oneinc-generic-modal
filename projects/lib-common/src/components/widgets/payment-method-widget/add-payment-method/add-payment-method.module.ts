import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// Components
import { AddPaymentMethodComponent } from './add-payment-method/add-payment-method.component';

// Modules
import { AddPaymentMethodRoutingModule } from './add-payment-method-routing.module';
import { ScreenflowRendererModule } from '@lib-common/components/screenflow-renderer';

// 3d party
import { NzButtonModule } from 'ng-zorro-antd';

@NgModule({
  declarations: [
    AddPaymentMethodComponent,
  ],
  imports: [
    CommonModule,
    AddPaymentMethodRoutingModule,
    ReactiveFormsModule,
    ScreenflowRendererModule,
    NzButtonModule,
  ],
})
export class AddPaymentMethodModule { }
