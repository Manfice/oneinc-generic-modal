import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddPaymentMethodComponent } from './add-payment-method/add-payment-method.component';

const routes: Routes = [
  {
    path: '',
    component: AddPaymentMethodComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddPaymentMethodRoutingModule { }
