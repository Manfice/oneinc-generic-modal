/** Bank account form value interface */
export interface IBankAccountFormValue {
  name: string;
  routingNumber: string;
  account: string;
  confirmAccount: string;
  savings: string;
  isDefault: boolean;
}
