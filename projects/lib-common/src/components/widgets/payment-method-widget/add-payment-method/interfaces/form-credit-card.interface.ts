/** Credit card form value interface */
export interface ICreditCardForm {
  name: string;
  cardNumber: string;
  expirationDate: string;
  code: string;
  address: string;
  zip: string;
  isDefault: boolean;
}
