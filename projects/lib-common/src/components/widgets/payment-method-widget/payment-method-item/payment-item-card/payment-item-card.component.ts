import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { IPayFromCard } from '@lib-common/interfaces';

@Component({
  selector: 'ol-payment-item-card',
  templateUrl: './payment-item-card.component.html',
  styleUrls: ['./payment-item-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentItemCardComponent implements OnInit {

  @Input() public method: IPayFromCard;

  public ngOnInit(): void {
  }
}
