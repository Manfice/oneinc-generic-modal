import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { IPayFromBankAccount } from '@lib-common/interfaces';

@Component({
  selector: 'ol-payment-item-bank',
  templateUrl: './payment-item-bank.component.html',
  styleUrls: ['./payment-item-bank.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentItemBankComponent implements OnInit {

  @Input() public method: IPayFromBankAccount;
  public ngOnInit(): void {
  }
}
