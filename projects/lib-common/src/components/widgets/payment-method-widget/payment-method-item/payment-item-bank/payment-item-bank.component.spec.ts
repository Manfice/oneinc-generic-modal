import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentItemBankComponent } from './payment-item-bank.component';

describe('PaymentItemBankComponent', () => {
  let component: PaymentItemBankComponent;
  let fixture: ComponentFixture<PaymentItemBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentItemBankComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentItemBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
