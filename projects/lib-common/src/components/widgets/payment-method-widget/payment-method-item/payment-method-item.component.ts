import { Component, OnInit, ChangeDetectionStrategy, Input, ChangeDetectorRef } from '@angular/core';

// Interfaces
import { IPaymentMethod } from '@lib-common/interfaces';

// Enums
import { PaymentTypeEnum } from '@lib-common/enums';
import { PaymentMethodService } from '../services';

@Component({
  selector: 'ol-payment-method-item',
  templateUrl: './payment-method-item.component.html',
  styleUrls: ['./payment-method-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentMethodItemComponent implements OnInit {
  @Input() public paymentMethod: IPaymentMethod;

  public paymentTypeEnum = PaymentTypeEnum;

  constructor(
    private readonly paymentMethodService: PaymentMethodService,
    private readonly cd: ChangeDetectorRef,
  ) {
  }

  public paymentMethodChanged(): void {
    this.paymentMethodService.paymentMethodChange$
      .next(this.paymentMethod.id);
  }

  public ngOnInit(): void {
    this.paymentMethodService.paymentMethodChange$
      .subscribe(() => this.cd.detectChanges());
  }
}
