import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Modules
import { WidgetHeadModule } from '@lib-common/components/widget-head';

// Components
import { PaymentMethodWidgetComponent } from './payment-method-widget/payment-method-widget.component';
import {
  PaymentMethodItemComponent,
  PaymentItemBankComponent,
  PaymentItemCardComponent,
} from './payment-method-item';

// 3d party
import { NzCheckboxModule, NzIconModule, NzButtonModule } from 'ng-zorro-antd';

@NgModule({
  declarations: [
    PaymentMethodWidgetComponent,
    PaymentMethodItemComponent,
    PaymentItemBankComponent,
    PaymentItemCardComponent,
  ],
  imports: [
    CommonModule,
    WidgetHeadModule,
    RouterModule,
    NzCheckboxModule,
    NzIconModule,
    NzButtonModule,
  ],
  providers: [
  ],
})
export class PaymentMethodWidgetModule { }
