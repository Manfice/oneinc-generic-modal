import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzIconModule, NzButtonModule } from 'ng-zorro-antd';

import { HeaderWidgetComponent } from './header-widget.component';

@NgModule({
  declarations: [
    HeaderWidgetComponent,
  ],
  imports: [
    CommonModule,
    NzIconModule,
    NzButtonModule,
  ],
})
export class HeaderWidgetModule { }
