import { Component, OnInit } from '@angular/core';

// Interfaces
import {
  IWidgetComponent,
  IHeaderWidgetData
} from '@lib-common/interfaces';

@Component({
  selector: 'ol-header-widget',
  templateUrl: './header-widget.component.html',
  styleUrls: ['./header-widget.component.scss'],
})
export class HeaderWidgetComponent implements OnInit, IWidgetComponent {
  public data: IHeaderWidgetData;

  public ngOnInit(): void {
  }
}
