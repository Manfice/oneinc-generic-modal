import { Type } from '@angular/core';

// Components
import { HeaderWidgetComponent } from '@lib-common/components/widgets/header-widget/header-widget.component';
import { PciDisclaimerWidgetComponent } from '@lib-common/components/widgets/pci-disclaimer-widget/pci-disclaimer-widget.component';
import { PaymentTypeWidgetComponent } from '@lib-common/components/widgets/payment-type-widget/payment-type-widget.component';
import { ControlWidgetComponent } from '@lib-common/components/widgets/controls-widget/control-widget/control-widget.component';
import { PaymentAmountWidgetComponent } from '@lib-common/components/widgets/payment-amount-widget/payment-amount-widget/payment-amount-widget.component';
import { PaymentMethodWidgetComponent } from '@lib-common/components/widgets/payment-method-widget/payment-method-widget/payment-method-widget.component';

export const widgets: Array<Type<any>> = [
  HeaderWidgetComponent,
  PciDisclaimerWidgetComponent,
  PaymentTypeWidgetComponent,
  ControlWidgetComponent,
  PaymentAmountWidgetComponent,
  PaymentMethodWidgetComponent,
];
