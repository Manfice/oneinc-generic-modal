import {
  HeaderWidgetModel,
  PciDisclaimerWidgetModel,
  PaymentTypeWidgetModel,
  WidgetBase,
  ControlWidgetModel,
  PaymentAmountWidgetModel,
  PaymentMethodWidgetModel,
} from '@lib-common/models';

export const WidgetModelListConstant: Array<WidgetBase> = [
  new HeaderWidgetModel(),
  new PciDisclaimerWidgetModel(),
  new PaymentTypeWidgetModel(),
  new ControlWidgetModel(),
  new PaymentAmountWidgetModel(),
  new PaymentMethodWidgetModel(),
];
