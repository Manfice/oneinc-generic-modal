export enum PaymentTypeIconsEnum {
  CreditCard = 'credit-card',
  BancAccount = 'bank',
  Cash = 'dollar',
}
