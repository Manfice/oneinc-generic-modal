export enum PaymentTypeEnum {
  CreditCard,
  BankAccount,
  Cash,
  ApplePay,
}
