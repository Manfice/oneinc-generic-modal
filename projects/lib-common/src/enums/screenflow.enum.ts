export enum ScreenflowTypeEnum {
  Pay = 'pay',
  AutoPay = 'autoPay',
  AddPaymentMethod = 'addPaymentMethod',
}
