import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetHostDirective } from './widget-host';

const directives = [
  WidgetHostDirective,
];

@NgModule({
  declarations: [
    ...directives,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ...directives,
  ],
})
export class OincDirectivesModule { }
