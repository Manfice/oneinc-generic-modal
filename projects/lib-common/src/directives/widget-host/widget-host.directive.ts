import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[olWidgetHost]',
})
export class WidgetHostDirective {

  constructor(
    public viewContainerRef: ViewContainerRef,
  ) { }
}
